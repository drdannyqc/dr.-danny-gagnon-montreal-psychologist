I value empowering people to make real changes in their lives in the ‘present-moment’ and not the past where unfortunate events happened, nor in the future as you can only plan for tomorrow in the ‘here-and-now’. Call +1 514-605-7610 for more information!

Address: 1310 Greene Avenue, Suite 760, Montreal, QC  H3Z 2B2, Canada

Phone: 514-605-7610
